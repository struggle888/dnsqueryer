// 核心功能： ID池
// 高并发、高频调用核心模块（100000次/秒），务必保证并发条件下的数据一致性&性能
// 二者缺一不可
// 功能: 根据ID列表初始化ID池、获取一个ID，给ID附上一个属性值，获取ID的属性值，释放一个ID

package utils

import (
	"errors"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

// ID池
// 适配高并发
// 功能:
// 获取ID，手动&自动释放ID
type LockMap struct {
	sync.RWMutex
	m map[uint16]int
}

type poolId struct {
	value      uint16
	expireTime int64
	status     uint32
}

type IDPool struct {
	pool chan int
	Property []interface{} // ID属性存储
	IDs      []poolId      // 目前ID列表
	IDMap    LockMap       // ID-index 反映射
}

var timeout = 30 * time.Second

func NewIDPool(ids []uint16) *IDPool {
	pool := IDPool{make(chan int, len(ids)), make([]interface{}, len(ids)), make([]poolId, len(ids)), LockMap{sync.RWMutex{}, make(map[uint16]int, len(ids))}}
	for i, id := range ids {
		pool.Property[i] = 0 // 临时赋到property里
		pool.pool <- i
		pool.IDMap.m[id] = i
		pool.IDs[i] = poolId{
			value:      id,
			expireTime: time.Now().Unix(),
			status:     0,
		}
	}
	go pool.checkExpire()
	return &pool
}

func (pool *IDPool) checkExpire() {
	for {
		time.Sleep(timeout / 50)
		for i := range pool.IDs {
			if atomic.LoadUint32(&pool.IDs[i].status) == 1 && pool.IDs[i].expireTime < time.Now().Unix() {
				pool.releaseID(i, true)
			}
		}
	}
}

// 获取一个ID，阻塞直到获取成功
func (pool *IDPool) GetID() uint16 {
	index := <- pool.pool
	pool.IDs[index].expireTime = time.Now().Add(timeout).Unix()
	atomic.StoreUint32(&pool.IDs[index].status, 1)
	return pool.IDs[index].value
}

// 设置ID的属性
func (pool *IDPool) SetProperty(id uint16, property interface{}) {
	index, ok := pool.getIndexByID(id)
	if !ok {
		panic(errors.New("error in set property"))
	}
	pool.Property[index] = property
}

// 获取ID的属性
func (pool *IDPool) GetProperty(id uint16) (property interface{}, ok bool) {
	index, ok := pool.getIndexByID(id)
	if !ok {
		return
	}
	property = pool.Property[index]
	return property, !(property == 0)
}

// 当收到返回报文后手动释放ID
func (pool *IDPool) ReleaseID(id uint16) {
	index, ok := pool.getIndexByID(id)
	if !ok {
		return
	}
	// 结束掉超时协程
	pool.releaseID(index, false)
}

func (pool *IDPool) releaseID(index int, timeout bool) {
	// 防止超时协程与收包协程同时触发
	if atomic.CompareAndSwapUint32(&pool.IDs[index].status, 1, 0) {
		pool.Property[index] = 0
	} else {
		return
	}

	// 如果ID超时了，那么换一个ID，防止超时报文后来又收到了 影响新的该ID的报文的接收
	if timeout {
		pool.IDMap.Lock()
		var newID uint16
		var ok bool
		for {
			newID = uint16(rand.Intn(65536))
			if _, ok = pool.IDMap.m[newID]; ok {
				continue
			}
			break
		}
		delete(pool.IDMap.m, pool.IDs[index].value)
		pool.IDs[index].value = newID
		pool.IDMap.m[newID] = index
		pool.IDMap.Unlock()
	}

	// 释放ID，推入队列中
	pool.pool <- index
}

// 根据ID获取Index
func (pool *IDPool) getIndexByID(id uint16) (index int, ok bool) {
	pool.IDMap.RLock()
	index, ok = pool.IDMap.m[id]
	pool.IDMap.RUnlock()
	return
}
