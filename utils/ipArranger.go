// 端口、ID分配器
package utils

import "hash/crc32"

type IPArranger struct {
	ports   []uint16
	idPools map[uint16]*IDPool
}

// New一个分配器，指定要使用的端口列表
func NewIPArranger(ports []uint16, idMultiple int) *IPArranger {
	arranger := IPArranger{ports: ports, idPools: make(map[uint16]*IDPool, len(ports))}
	// 为每一个端口分配ID池
	// 目的：使每个端口的ID尽量不相同，以最大程度上识别出错误包、干扰包、异常包
	for index, port := range ports {
		var ids []uint16
		// i的自增值为len(ports)时，为完美情况，即将65536个ID完全不重复的分配给所有端口
		// 然而，所有端口总共使用65536个ID十分影响性能（ID池太小了）
		// 所以我们适当扩充ID池的大小来在 稍微增加错误率的情况下，显著提升性能
		// 这里除50，即扩大ID池到原先的50倍，错误率也提升50倍
		// 对于异常数据包判定成功率 = 1 - 1/[使用的端口数]*ID池扩大的倍数
		// 一般使用几万个端口，成功率已经很高了
		for i := index; i < 65536; i += len(ports) / idMultiple {
			ids = append(ids, uint16(i))
		}
		arranger.idPools[port] = NewIDPool(ids)
	}
	return &arranger
}

// 根据IP随机分配一个端口：哈希掉IP然后模一下端口总数即可
func (arranger *IPArranger) GetPortFromIP(ip string) uint16 {
	return arranger.ports[crc32.ChecksumIEEE([]byte(ip))%uint32(len(arranger.ports))]
}

// 下面几个都是通过端口取到对应的ID池，然后执行关于ID的操作
func (arranger *IPArranger) GetID(port uint16) uint16 {
	return arranger.idPools[port].GetID()
}

func (arranger *IPArranger) ReleaseID(port uint16, id uint16) {
	arranger.idPools[port].ReleaseID(id)
}

func (arranger *IPArranger) SetProperty(port uint16, id uint16, property interface{}) {
	arranger.idPools[port].SetProperty(id, property)
}

func (arranger *IPArranger) GetProperty(port uint16, id uint16) (property interface{}, has bool, legal bool) {
	// 可以加通过遍历ID池中目前的ID列表来直接判断数据包是否合法，实际需求不用总结出非法数据包，就注释掉了
	// 检查端口-id规则
	//for _, d := range arranger.idPools[port].IDs{
	//	if d == id{
	//		property, has = arranger.idPools[port].GetProperty(id)
	//		return property, has, true
	//	}
	//}
	//return nil, false, false
	property, has = arranger.idPools[port].GetProperty(id)
	return property, has, true
}
