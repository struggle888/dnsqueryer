package main

import (
	"bitbucket.org/struggle888/globaldns/utils" // 工具函数地址，自行修改
	tools "bitbucket.org/struggle888/go-utils"
	"bufio"
	"flag"
	"fmt"
	"github.com/miekg/dns"
	"github.com/rotisserie/eris"
	"go.uber.org/zap"
	"golang.org/x/net/ipv4"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

/* 配置信息 */
var (
	// Sender数目
	workerAmount int
	// 使用端口数量
	portAmount int
	// 发包倍数
	idMultiple int

	// 速度限制
	speedLimit int

	srcIPString string
	srcIP       net.IP

	// 仅显示帮助菜单
	help bool
	mode string

	// 手动指定目标请求域名列表
	targetString string
	targetFile   string
	targets      []string

	// 请求的DNS服务器列表
	dnsServerString string
	dnsFile         string
	dnsServers      []string

	// 校验模式时，期望的IP
	exceptIP string

	//输出列
	outputDomain bool
	outputIP     bool
	outputSrc    bool

	// 其它非命令行参数
	// 状态
	outputClosed bool

	domainMap map[string]struct{}
)

func init() {
	// 运行模式
	flag.StringVar(&mode, "mode", "query", "运行模式，可选query或check")

	// 帮助
	flag.BoolVar(&help, "help", false, "显示帮助菜单。默认: false")
	flag.BoolVar(&help, "h", false, "【缩写】显示帮助菜单。默认: false")

	// 扫描配置
	flag.IntVar(&workerAmount, "sender", 6, "Sender协程数量，推荐4c8g:6, 2c4g:2")
	flag.IntVar(&workerAmount, "s", 6, "【缩写】Sender协程数量，推荐4c8g:6, 2c4g:2")
	flag.IntVar(&portAmount, "ports", 10000, "使用的端口数")
	flag.IntVar(&portAmount, "p", 10000, "【缩写】使用的端口数")
	flag.IntVar(&idMultiple, "multiple", 50, "ID池倍数，直接影响发包速率")
	flag.IntVar(&idMultiple, "m", 50, "【缩写】ID池倍数，直接影响发包速率")
	flag.StringVar(&srcIPString, "src", "", "src IP string")
	flag.IntVar(&speedLimit, "rate", 100000, "每秒发包速度限制")

	// 请求域名配置
	flag.StringVar(&targetString, "t", "", "目标域名，用逗号分隔的域名字符串")
	flag.StringVar(&targetString, "target", "", "【缩写】目标域名，用逗号分隔的域名字符串")
	flag.StringVar(&targetFile, "targetFile", "", "目标域名输入文件路径")
	flag.StringVar(&targetFile, "tf", "", "【缩写】目标域名输入文件路径")

	// 请求的DNS服务器
	flag.StringVar(&dnsServerString, "dns", "", "输入的DNS服务器，用逗号分隔的IP字符串")
	flag.StringVar(&dnsFile, "dnsFile", "", "输入的DNS服务器文件路径")
	flag.StringVar(&dnsFile, "df", "", "【缩写】输入的DNS服务器文件路径")

	// 输出列
	flag.BoolVar(&outputDomain, "od", false, "输出请求域名")
	flag.BoolVar(&outputIP, "oi", true, "输出结果IP")
	flag.BoolVar(&outputSrc, "os", false, "输出DNS服务器IP")

	// check模式下专属
	flag.StringVar(&exceptIP, "except", "", "校验模式下期望IP")
}

func parseParams() bool {
	var err error
	rand.Seed(time.Now().Unix())

	flag.Parse()
	if help {
		flag.Usage()
		return false
	}

	srcIP = net.ParseIP(srcIPString)

	if mode != "query" && mode != "check" {
		logger.Error("Running mode should be query or check!")
		return false
	}

	if targetFile != "" {
		targets, err = tools.ReadStringsFromFile(targetFile)
		if err != nil {
			logger.Error(eris.Wrapf(err, "Error in reading target file: %s", targetFile))
			return false
		}
	} else {
		targets = strings.Split(targetString, ",")
	}

	if dnsFile != "" {
		dnsServers, err = tools.ReadStringsFromFile(dnsFile)
		if err != nil {
			logger.Error(eris.Wrapf(err, "Error in reading dns file: %s", dnsFile))
			return false
		}
	} else {
		dnsServers = strings.Split(dnsServerString, ",")
	}

	domainMap = make(map[string]struct{}, len(targets))
	for _, domain := range targets {
		domainMap[domain] = struct{}{}
	}
	return true
}

// 统计信息，变量以count开头命名
var (
	countSent    int64
	countResult1 int
	countResult2 int
	countResult3 int
)

func statistics() {
	var n int
	fmt.Printf("Average Speed:\n[Package Sent] | [Common Result] | [PTR Result] | [SelfDomain Result] | [Running Time]\n")
	for outputClosed == false {
		time.Sleep(time.Second)
		n++
		fmt.Printf("%14d | %15d | %12d | %19d | %13ds\r", countSent/int64(n), countResult1/n, countResult2/n, countResult3/n, n)
	}
}

var lastSecondSentCounter uint32

func clearCounter() {
	for outputClosed == false {
		lastSecondSentCounter = 0
		time.Sleep(time.Second / 10)
	}
}

// 常量
var logger = initLogger()

// 结果结构体
type DNSResult struct {
	domain string
	ip     string
	src    string
}

type DNSRequest struct {
	server string // 使用的DNS服务器
	domain string // 查询的域名
}

// sender
func DNSSender(input <-chan DNSRequest, arranger *utils.IPArranger, finish chan<- bool) {
	conn, err := net.ListenPacket("ip4:udp", "0.0.0.0")
	if err != nil {
		logger.Fatal(err)
	}
	c, err := ipv4.NewRawConn(conn)
	if err != nil {
		logger.Fatal(err)
	}
	defer c.Close()
	defer conn.Close()
	msg := new(dns.Msg)
	msg.Question = make([]dns.Question, 1)
	msg.Question[0] = dns.Question{Qtype: dns.TypeA, Qclass: dns.ClassINET}
	msg.RecursionDesired = true
	for {
		request, ok := <-input
		if ok == false { // 输入管道关闭
			break
		}
		// 获取这个IP应该从哪个端口发
		port := arranger.GetPortFromIP(request.server)
		// 申请一个ID
		msg.Id = arranger.GetID(port)
		msg.Question[0].Name = dns.Fqdn(request.domain)
		m, err := msg.Pack()
		if err != nil {
			panic(err)
		}

		// 绑定 port-id-request
		arranger.SetProperty(port, msg.Id, &request)
		ipHeader, udpData := buildHeader(m, net.ParseIP(request.server), port)
		if err := c.WriteTo(ipHeader, udpData, nil); err != nil {
			logger.Error(err)
		}
		atomic.AddInt64(&countSent, 1)
		atomic.AddUint32(&lastSecondSentCounter, 1)
		for atomic.LoadUint32(&lastSecondSentCounter) > uint32(speedLimit)/10 {
			time.Sleep(time.Microsecond)
		}
	}
	finish <- true

}

// 接收包
func DNSReceiver(c *ipv4.PacketConn, port uint16, output chan<- DNSResult, arranger *utils.IPArranger) {
	resp := new(dns.Msg)
	for {
		b := make([]byte, 2000)
		_, _, _, err := c.ReadFrom(b)
		if err != nil {
			if err, ok := err.(net.Error); ok && err.Timeout() {
				continue
			}
			logger.Debug("Error in reading response packet:", err)
			continue
		}
		err = resp.Unpack(b)
		if err != nil {
			logger.Debug("Error in unpack response:", err)
			continue
		}
		// 通过ID获取请求
		request, ok, _ := arranger.GetProperty(port, resp.Id)
		if !ok { // 这个ID不是之前申请的，即此数据包为超时包或其它错误包，丢弃
			continue
		}
		arranger.ReleaseID(port, resp.Id)

		aResult := make(map[string]map[string]struct{})
		for _, answer := range resp.Answer {
			if a, ok := answer.(*dns.A); ok {
				if _, ok := domainMap[a.Header().Name]; ok {
					if mode == "query" || a.A.String() == exceptIP {
						if _, ok := aResult[a.Hdr.Name]; !ok {
							aResult[a.Hdr.Name] = make(map[string]struct{})
						}
						aResult[a.Hdr.Name][a.A.String()] = struct{}{}
					}
				}
			}
		}

		// 生成结果
		if outputClosed {
			break
		}

		for domain := range aResult {
			for ip := range aResult[domain] {
				output <- DNSResult{
					domain: domain,
					ip:     ip,
					src:    request.(*DNSRequest).server,
				}
			}
		}
	}
}

// 接收结果输出到文件
func OutputHandler(output <-chan DNSResult, finish chan<- bool) {
	f, err := os.Create("dnsQueryResult.txt")
	if err != nil {
		logger.Fatal(eris.Wrapf(err, "Error in creating result file"))
	}
	writer := bufio.NewWriter(f)
	for {
		result, ok := <-output
		if ok == false {
			break
		}
		var temp []string
		if outputDomain {
			temp = append(temp, result.domain)
		}
		if outputIP {
			temp = append(temp, result.ip)
		}
		if outputSrc {
			temp = append(temp, result.src)
		}
		_, err := writer.WriteString(strings.Join(temp, " ") + "\n")
		if err != nil {
			logger.Fatal(err)
		}
	}
	err = writer.Flush()
	if err != nil {
		logger.Fatal(err)
	}
	err = f.Close()
	if err != nil {
		logger.Fatal(err)
	}
	finish <- true
}

func TaskProducer(input chan<- DNSRequest) {
	size := uint32(len(targets) * len(dnsServers))
	shuffler := tools.NewShuffler(size, []byte(strconv.Itoa(rand.Intn(100000000))))
	var i uint32
	for i = 0; i < size; i++ {
		n := int(shuffler.Get(i))
		domain := targets[n/len(dnsServers)]
		server := dnsServers[n%len(dnsServers)]
		input <- DNSRequest{
			server: server,
			domain: domain,
		}
	}
	// 等待120s
	logger.Infof("Sent ALL!")
	close(input)
}

func run() {
	/* 初始化参数 */
	logger.Info("初始化中...")

	/* 初始化管道 */
	input := make(chan DNSRequest, 1000*(workerAmount)) // 输入管道
	output := make(chan DNSResult, 100*(portAmount))    // 结果输出管道
	finish := make(chan bool, workerAmount+1)           // 结束信息管道

	/* 启动 */

	// 初始化连接

	ports := make([]uint16, portAmount)
	deduplicate := map[uint16]struct{}{}
	netConnections := make(map[uint16]*net.PacketConn, portAmount)
	packetConnections := make(map[uint16]*ipv4.PacketConn, portAmount)
	for i := 0; i < portAmount; i++ {
		for {
			port := uint16(rand.Intn(65536-10000) + 10000) // 随机生成端口，尝试UDP占用，如果重复或者无法占用，重试
			if _, ok := deduplicate[port]; !ok {
				conn, err := net.ListenPacket("udp4", "0.0.0.0:"+strconv.Itoa(int(port)))
				if err != nil {
					logger.Fatal(err)
				}
				if err != nil {
					if strings.Contains(err.Error(), "address already in use") {
						continue
					} else {
						logger.Fatal(err)
					}
				}
				deduplicate[port] = struct{}{}
				ports[i] = port
				netConnections[port] = &conn
				packetConnections[port] = ipv4.NewPacketConn(*netConnections[port])
				if err != nil {
					logger.Fatal(err)
				}
				defer packetConnections[port].Close()
				defer (*netConnections[port]).Close()
				break
			}
		}
	}

	// 启动
	logger.Info("启动~")
	arranger := utils.NewIPArranger(ports, idMultiple) // 核心组件，IP-PORT-ID分配器
	for _, port := range ports {                       // 多少个端口就多少个接收器
		go DNSReceiver(packetConnections[port], port, output, arranger)
	}
	go TaskProducer(input)           // 启动数据输入
	go OutputHandler(output, finish) // 启动数据输出处理协程
	go statistics()
	go clearCounter()
	for i := 0; i < workerAmount; i++ { // 由于是无阻塞发包，发包协程个位数即可
		go DNSSender(input, arranger, finish)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finish
	}
	logger.Info("worker已完成全部任务")
	time.Sleep(30 * time.Second)
	outputClosed = true
	close(output) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	logger.Info("等待剩余结果写入完成")
	<-finish //输出处理协程结束

	/* 结束 */
	close(finish)
	logger.Info("完成！")
	// 程序打印 “完成” 后有可能卡死不动，个人怀疑是上面的defer导致的，Ctrl+C即可
}

func main() {
	if !parseParams() {
		return
	}
	run()
}

// 以下为工具函数

//初始化日志模块，默认等级为info
func initLogger() *zap.SugaredLogger {
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	return logger.Sugar()
}

func buildHeader(buff []byte, targetIP net.IP, port uint16) (*ipv4.Header, []byte) {
	ipHeader := &ipv4.Header{
		Version: ipv4.Version,
		//IP头长一般是20
		Len: ipv4.HeaderLen,
		TOS: 0x24,
		//buff为数据
		TTL:      255,
		Flags:    ipv4.DontFragment,
		FragOff:  0,
		Protocol: 17,
		Src:      srcIP,
		Dst:      targetIP,         //目的ip
		ID:       rand.Intn(65536), // ID
	}

	udpHeader := make([]byte, 20)
	udpHeader[0], udpHeader[1], udpHeader[2], udpHeader[3] = srcIP[12], srcIP[13], srcIP[14], srcIP[15]
	udpHeader[4], udpHeader[5], udpHeader[6], udpHeader[7] = targetIP[12], targetIP[13], targetIP[14], targetIP[15]
	udpHeader[8], udpHeader[9] = 0x00, 0x11
	udpHeader[10], udpHeader[11] = 0x00, byte(len(buff)+8)
	// 12 13 源端口 14 15目的端口
	udpHeader[12], udpHeader[13] = byte(port>>8), byte(port&0xff)
	udpHeader[14], udpHeader[15] = 0, 53
	udpHeader[16], udpHeader[17] = 0x00, byte(len(buff)+8)
	udpHeader[18], udpHeader[19] = 0x00, 0x00
	check := tools.CheckSum(append(udpHeader, buff...))
	udpHeader[18], udpHeader[19] = byte(check>>8&255), byte(check&255)
	return ipHeader, append(udpHeader[12:20], buff...)
}
